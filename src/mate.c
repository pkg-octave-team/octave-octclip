/* -*- coding: utf-8 -*- */
/**
\ingroup anespec gshhs mate legendre
@{
\file mate.c
\brief Definición de funciones para la realización de cálculos matemáticos
       generales.
\author José Luis García Pallero, jgpallero@gmail.com
\date 17 de mayo de 2010
\copyright
Copyright (c) 2009-2016, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#include"libgeoc/mate.h"
/******************************************************************************/
/******************************************************************************/
/**
\brief Array que almacena de manera explícita los factoriales de los números que
       van de \em 0 a #GEOC_MATE_CONST_LDBL_NMAXFAC.

       Este array se basa en el array \p fact_table, que se puede encontrar en
       el fichero \p gamma.c, de la biblioteca GSL.
*/
static __mateFactExpl __tablaFacExpl[GEOC_MATE_CONST_LDBL_NMAXFAC+1]=
{
    { 0,  1.0                                     },
    { 1,  1.0                                     },
    { 2,  2.0                                     },
    { 3,  6.0                                     },
    { 4,  24.0                                    },
    { 5,  120.0                                   },
    { 6,  720.0                                   },
    { 7,  5040.0                                  },
    { 8,  40320.0                                 },
    { 9,  362880.0                                },
    { 10, 3628800.0                               },
    { 11, 39916800.0                              },
    { 12, 479001600.0                             },
    { 13, 6227020800.0                            },
    { 14, 87178291200.0                           },
    { 15, 1307674368000.0                         },
    { 16, 20922789888000.0                        },
    { 17, 355687428096000.0                       },
    { 18, 6402373705728000.0                      },
    { 19, 121645100408832000.0                    },
    { 20, 2432902008176640000.0                   },
    { 21, 51090942171709440000.0                  },
    { 22, 1124000727777607680000.0                },
    { 23, 25852016738884976640000.0               },
    { 24, 620448401733239439360000.0              },
    { 25, 15511210043330985984000000.0            },
    { 26, 403291461126605635584000000.0           },
    { 27, 10888869450418352160768000000.0         },
    { 28, 304888344611713860501504000000.0        },
    { 29, 8841761993739701954543616000000.0       },
    { 30, 265252859812191058636308480000000.0     },
    { 31, 8222838654177922817725562880000000.0    },
    { 32, 263130836933693530167218012160000000.0  },
    { 33, 8683317618811886495518194401280000000.0 },
    { 34, 2.95232799039604140847618609644e38      },
    { 35, 1.03331479663861449296666513375e40      },
    { 36, 3.71993326789901217467999448151e41      },
    { 37, 1.37637530912263450463159795816e43      },
    { 38, 5.23022617466601111760007224100e44      },
    { 39, 2.03978820811974433586402817399e46      },
    { 40, 8.15915283247897734345611269600e47      },
    { 41, 3.34525266131638071081700620534e49      },
    { 42, 1.40500611775287989854314260624e51      },
    { 43, 6.04152630633738356373551320685e52      },
    { 44, 2.65827157478844876804362581101e54      },
    { 45, 1.19622220865480194561963161496e56      },
    { 46, 5.50262215981208894985030542880e57      },
    { 47, 2.58623241511168180642964355154e59      },
    { 48, 1.24139155925360726708622890474e61      },
    { 49, 6.08281864034267560872252163321e62      },
    { 50, 3.04140932017133780436126081661e64      },
    { 51, 1.55111875328738228022424301647e66      },
    { 52, 8.06581751709438785716606368564e67      },
    { 53, 4.27488328406002556429801375339e69      },
    { 54, 2.30843697339241380472092742683e71      },
    { 55, 1.26964033536582759259651008476e73      },
    { 56, 7.10998587804863451854045647464e74      },
    { 57, 4.05269195048772167556806019054e76      },
    { 58, 2.35056133128287857182947491052e78      },
    { 59, 1.38683118545689835737939019720e80      },
    { 60, 8.32098711274139014427634118320e81      },
    { 61, 5.07580213877224798800856812177e83      },
    { 62, 3.14699732603879375256531223550e85      },
    { 63, 1.982608315404440064116146708360e87     },
    { 64, 1.268869321858841641034333893350e89     },
    { 65, 8.247650592082470666723170306800e90     },
    { 66, 5.443449390774430640037292402480e92     },
    { 67, 3.647111091818868528824985909660e94     },
    { 68, 2.480035542436830599600990418570e96     },
    { 69, 1.711224524281413113724683388810e98     },
    { 70, 1.197857166996989179607278372170e100    },
    { 71, 8.504785885678623175211676442400e101    },
    { 72, 6.123445837688608686152407038530e103    },
    { 73, 4.470115461512684340891257138130e105    },
    { 74, 3.307885441519386412259530282210e107    },
    { 75, 2.480914081139539809194647711660e109    },
    { 76, 1.885494701666050254987932260860e111    },
    { 77, 1.451830920282858696340707840860e113    },
    { 78, 1.132428117820629783145752115870e115    },
    { 79, 8.946182130782975286851441715400e116    },
    { 80, 7.156945704626380229481153372320e118    },
    { 81, 5.797126020747367985879734231580e120    },
    { 82, 4.753643337012841748421382069890e122    },
    { 83, 3.945523969720658651189747118010e124    },
    { 84, 3.314240134565353266999387579130e126    },
    { 85, 2.817104114380550276949479442260e128    },
    { 86, 2.422709538367273238176552320340e130    },
    { 87, 2.107757298379527717213600518700e132    },
    { 88, 1.854826422573984391147968456460e134    },
    { 89, 1.650795516090846108121691926250e136    },
    { 90, 1.485715964481761497309522733620e138    },
    { 91, 1.352001527678402962551665687590e140    },
    { 92, 1.243841405464130725547532432590e142    },
    { 93, 1.156772507081641574759205162310e144    },
    { 94, 1.087366156656743080273652852570e146    },
    { 95, 1.032997848823905926259970209940e148    },
    { 96, 9.916779348709496892095714015400e149    },
    { 97, 9.619275968248211985332842594960e151    },
    { 98, 9.426890448883247745626185743100e153    },
    { 99, 9.332621544394415268169923885600e155    },
    { 100,9.33262154439441526816992388563e157     },
    { 101,9.42594775983835942085162312450e159     },
    { 102,9.61446671503512660926865558700e161     },
    { 103,9.90290071648618040754671525458e163     },
    { 104,1.02990167451456276238485838648e166     },
    { 105,1.08139675824029090050410130580e168     },
    { 106,1.146280563734708354534347384148e170    },
    { 107,1.226520203196137939351751701040e172    },
    { 108,1.324641819451828974499891837120e174    },
    { 109,1.443859583202493582204882102460e176    },
    { 110,1.588245541522742940425370312710e178    },
    { 111,1.762952551090244663872161047110e180    },
    { 112,1.974506857221074023536820372760e182    },
    { 113,2.231192748659813646596607021220e184    },
    { 114,2.543559733472187557120132004190e186    },
    { 115,2.925093693493015690688151804820e188    },
    { 116,3.393108684451898201198256093590e190    },
    { 117,3.96993716080872089540195962950e192     },
    { 118,4.68452584975429065657431236281e194     },
    { 119,5.57458576120760588132343171174e196     },
    { 120,6.68950291344912705758811805409e198     },
    { 121,8.09429852527344373968162284545e200     },
    { 122,9.87504420083360136241157987140e202     },
    { 123,1.21463043670253296757662432419e205     },
    { 124,1.50614174151114087979501416199e207     },
    { 125,1.88267717688892609974376770249e209     },
    { 126,2.37217324288004688567714730514e211     },
    { 127,3.01266001845765954480997707753e213     },
    { 128,3.85620482362580421735677065923e215     },
    { 129,4.97450422247728744039023415041e217     },
    { 130,6.46685548922047367250730439554e219     },
    { 131,8.47158069087882051098456875820e221     },
    { 132,1.11824865119600430744996307608e224     },
    { 133,1.48727070609068572890845089118e226     },
    { 134,1.99294274616151887673732419418e228     },
    { 135,2.69047270731805048359538766215e230     },
    { 136,3.65904288195254865768972722052e232     },
    { 137,5.01288874827499166103492629211e234     },
    { 138,6.91778647261948849222819828311e236     },
    { 139,9.61572319694108900419719561353e238     },
    { 140,1.34620124757175246058760738589e241     },
    { 141,1.89814375907617096942852641411e243     },
    { 142,2.69536413788816277658850750804e245     },
    { 143,3.85437071718007277052156573649e247     },
    { 144,5.55029383273930478955105466055e249     },
    { 145,8.04792605747199194484902925780e251     },
    { 146,1.17499720439091082394795827164e254     },
    { 147,1.72724589045463891120349865931e256     },
    { 148,2.55632391787286558858117801578e258     },
    { 149,3.80892263763056972698595524351e260     },
    { 150,5.71338395644585459047893286526e262     },
    { 151,8.62720977423324043162318862650e264     },
    { 152,1.31133588568345254560672467123e267     },
    { 153,2.00634390509568239477828874699e269     },
    { 154,3.08976961384735088795856467036e271     },
    { 155,4.78914290146339387633577523906e273     },
    { 156,7.47106292628289444708380937294e275     },
    { 157,1.17295687942641442819215807155e278     },
    { 158,1.85327186949373479654360975305e280     },
    { 159,2.94670227249503832650433950735e282     },
    { 160,4.71472363599206132240694321176e284     },
    { 161,7.59070505394721872907517857094e286     },
    { 162,1.22969421873944943411017892849e289     },
    { 163,2.00440157654530257759959165344e291     },
    { 164,3.28721858553429622726333031164e293     },
    { 165,5.42391066613158877498449501421e295     },
    { 166,9.00369170577843736647426172359e297     },
    { 167,1.50361651486499904020120170784e300     },
    { 168,2.52607574497319838753801886917e302     },
    { 169,4.26906800900470527493925188890e304     },
    { 170,7.25741561530799896739672821113e306     },
    { 171,1.24101807021766782342484052410e309L    },
    { 172,2.13455108077438865629072570146e311L    },
    { 173,3.69277336973969237538295546352e313L    },
    { 174,6.42542566334706473316634250653e315L    },
    { 175,1.12444949108573632830410993864e318L    },
    { 176,1.97903110431089593781523349201e320L    },
    { 177,3.50288505463028580993296328086e322L    },
    { 178,6.23513539724190874168067463993e324L    },
    { 179,1.11608923610630166476084076055e327L    },
    { 180,2.00896062499134299656951336898e329L    },
    { 181,3.63621873123433082379081919786e331L    },
    { 182,6.61791809084648209929929094011e333L    },
    { 183,1.21107901062490622417177024204e336L    },
    { 184,2.22838537954982745247605724535e338L    },
    { 185,4.12251295216718078708070590390e340L    },
    { 186,7.66787409103095626397011298130e342L    },
    { 187,1.43389245502278882136241112750e345L    },
    { 188,2.69571781544284298416133291969e347L    },
    { 189,5.09490667118697324006491921822e349L    },
    { 190,9.68032267525524915612334651460e351L    },
    { 191,1.84894163097375258881955918429e354L    },
    { 192,3.54996793146960497053355363384e356L    },
    { 193,6.85143810773633759312975851330e358L    },
    { 194,1.32917899290084949306717315158e361L    },
    { 195,2.59189903615665651148098764559e363L    },
    { 196,5.08012211086704676250273578535e365L    },
    { 197,1.00078405584080821221303894971e368L    },
    { 198,1.98155243056480026018181712043e370L    },
    { 199,3.94328933682395251776181606966e372L    },
    { 200,7.88657867364790503552363213932e374L    }
};
/******************************************************************************/
/******************************************************************************/
int GeocTipoCalcProd(void)
{
    //distingimos los tipos de cálculo
#if defined(CALCULO_PRODUCTO_MULT)
    return GEOC_PROD_MULT;
#elif defined(CALCULO_PRODUCTO_LOG)
    return GEOC_PROD_LOG;
#else
    #error *****No se ha definido el método de cálculo de la función Producto()
#endif
}
/******************************************************************************/
/******************************************************************************/
double Media(const double* datos,
             const size_t nDatos,
             const int inc)
{
    //índice para recorrer un bucle
    size_t i=0;
    //posición del elemento de trabajo
    size_t pos=0;
    //variable para almacenar el resultado
    double resultado=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la posición del elemento inicial del vector
    pos = GEOC_INICIO_VEC(nDatos,inc);
    //recorremos el vector
    for(i=0;i<nDatos;i++)
    {
        //vamos sumando los valores del vector
        resultado += datos[pos];
        //aumentamos el contador de posiciones
        pos = inc>0 ? pos+(size_t)inc : pos-(size_t)abs(inc);
    }
    //calculamos la media
    resultado /= (double)nDatos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double Varianza(const double* datos,
                const size_t nDatos,
                const int inc,
                const double media)
{
    //índice para recorrer un bucle
    size_t i=0;
    //posición del elemento de trabajo
    size_t pos=0;
    //residuo
    double res=0.0;
    //variable para almacenar el resultado
    double resultado=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la posición del elemento inicial del vector
    pos = GEOC_INICIO_VEC(nDatos,inc);
    //recorremos el vector
    for(i=0;i<nDatos;i++)
    {
        //calculamos el residuo
        res = datos[pos]-media;
        //vamos sumando los cuadrados del valor menos la media
        resultado += res*res;
        //aumentamos el contador de posiciones
        pos = inc>0 ? pos+(size_t)inc : pos-(size_t)abs(inc);
    }
    //calculamos la varianza
    resultado /= (double)(nDatos-1);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double MediaPonderada(const double* datos,
                      const size_t nDatos,
                      const int incDatos,
                      const double* pesos,
                      const int incPesos)
{
    //índice para recorrer un bucle
    size_t i=0;
    //posiciones de los elementos de trabajo
    size_t posDatos=0,posPesos;
    //variable auxiliar
    double sumaPesos=0.0;
    //variable para almacenar el resultado
    double resultado=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la posición del elemento inicial de los vectores
    posDatos = GEOC_INICIO_VEC(nDatos,incDatos);
    posPesos = GEOC_INICIO_VEC(nDatos,incPesos);
    //recorremos el vector
    for(i=0;i<nDatos;i++)
    {
        //vamos sumando los valores del vector
        resultado += datos[posDatos]*pesos[posPesos];
        //aumentamos el contador de posiciones
        posDatos = incDatos>0 ? posDatos+(size_t)incDatos
                              : posDatos-(size_t)abs(incDatos);
        posPesos = incPesos>0 ? posPesos+(size_t)incPesos
                              : posPesos-(size_t)abs(incPesos);
        //vamos sumando los pesos
        sumaPesos += pesos[posPesos];
    }
    //calculamos la media
    resultado /= sumaPesos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double Mediana(const double* datos,
               const size_t nDatos,
               const int inc)
{
    //posición del elemento central
    size_t pos=0;
    //posición de inicio del vector
    size_t posInicio;
    //variable auxiliar
    size_t posAux=0;
    //variable para almacenar el resultado
    double resultado=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la posición del elemento inicial del vector
    posInicio = GEOC_INICIO_VEC(nDatos,inc);
    //distinguimos entre número par o impar de datos
    if(nDatos%2)
    {
        //el número de datos es impar, calculamos la posición del punto medio
        pos = (size_t)ceil((double)nDatos/2.0)-1;
        //calculamos la posición en memoria
        pos = inc>0 ? posInicio+pos*(size_t)inc
                    : posInicio-pos*(size_t)abs(inc);
        //extraemos el dato
        resultado = datos[pos];
    }
    else
    {
        //el número de datos es par, calculamos la posición de los puntos medios
        pos = (size_t)((double)nDatos/2.0)-1;
        //calculamos la posición en memoria
        pos = inc>0 ? posInicio+pos*(size_t)inc
                    : posInicio-pos*(size_t)abs(inc);
        //calculamos la posición del siguiente elemento del vector
        posAux = inc>0 ? pos+(size_t)inc : pos-(size_t)abs(inc);
        //calculamos la mediana
        resultado = (datos[pos]+datos[posAux])/2.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double ProductoMult(size_t inicio,
                    size_t fin)
{
    //variables intermedias
    size_t menor=0.0,mayor=0.0;
    //variable para almacenar el resultado
    double resultado=1.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema de forma iterativa
    if((inicio==0)||(fin==0))
    {
        resultado = 0.0;
    }
    else
    {
        //seleccionamos los elementos mayor y menor
        menor = inicio<=fin ? inicio : fin;
        mayor = inicio>fin  ? inicio : fin;
        //multiplicamos
        while(menor<=mayor)
        {
            resultado *= (double)menor;
            menor++;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
long double ProductoMultLD(size_t inicio,
                           size_t fin)
{
    //variables intermedias
    size_t menor=0.0,mayor=0.0;
    //variable para almacenar el resultado
    long double resultado=1.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema de forma iterativa
    if((inicio==0)||(fin==0))
    {
        resultado = 0.0;
    }
    else
    {
        //seleccionamos los elementos mayor y menor
        menor = inicio<=fin ? inicio : fin;
        mayor = inicio>fin  ? inicio : fin;
        //multiplicamos
        while(menor<=mayor)
        {
            resultado *= (long double)menor;
            menor++;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double ProductoLog(size_t inicio,
                   size_t fin)
{
    //variables intermedias
    size_t menor=0.0,mayor=0.0;
    //variable para almacenar el resultado
    double resultado=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema de forma iterativa
    if((inicio==0)||(fin==0))
    {
        resultado = 0.0;
    }
    else
    {
        //seleccionamos los elementos mayor y menor
        menor = inicio<=fin ? inicio : fin;
        mayor = inicio>fin  ? inicio : fin;
        //multiplicamos
        while(menor<=mayor)
        {
            resultado += log((double)menor);
            menor++;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return round(pow(GEOC_CONST_E,resultado));
}
/******************************************************************************/
/******************************************************************************/
long double ProductoLogLD(size_t inicio,
                          size_t fin)
{
    //variables intermedias
    size_t menor=0.0,mayor=0.0;
    //variable para almacenar el resultado
    long double resultado=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema de forma iterativa
    if((inicio==0)||(fin==0))
    {
        resultado = 0.0;
    }
    else
    {
        //seleccionamos los elementos mayor y menor
        menor = inicio<=fin ? inicio : fin;
        mayor = inicio>fin  ? inicio : fin;
        //multiplicamos
        while(menor<=mayor)
        {
            resultado += (long double)log((double)menor);
            menor++;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return roundl(powl((long double)GEOC_CONST_E,resultado));
}
/******************************************************************************/
/******************************************************************************/
double Producto(size_t inicio,
                size_t fin)
{
    //variable para almacenar el resultado
    double resultado=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //método de cálculo para números mayores que GEOC_MATE_CONST_NMAXFAC
#if defined(CALCULO_PRODUCTO_MULT)
    resultado = ProductoMult(inicio,fin);
#elif defined(CALCULO_PRODUCTO_LOG)
    resultado = ProductoLog(inicio,fin);
#else
    #error *****No se ha definido el método de cálculo de la función Producto()
#endif
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
long double ProductoLD(size_t inicio,
                       size_t fin)
{
    //variable para almacenar el resultado
    long double resultado=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //método de cálculo para números mayores que GEOC_MATE_CONST_NMAXFAC
#if defined(CALCULO_PRODUCTO_MULT)
    resultado = ProductoMultLD(inicio,fin);
#elif defined(CALCULO_PRODUCTO_LOG)
    resultado = ProductoLogLD(inicio,fin);
#else
    #error *****No se ha definido el método de cálculo de la función Producto()
#endif
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double FactorialMult(size_t numero)
{
    //variable para almacenar el resultado
    double resultado=1.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema
    if(numero)
    {
        resultado = ProductoMult((size_t)1,numero);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
long double FactorialMultLD(size_t numero)
{
    //variable para almacenar el resultado
    long double resultado=1.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema
    if(numero)
    {
        resultado = ProductoMultLD((size_t)1,numero);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double FactorialLog(size_t numero)
{
    //variable para almacenar el resultado
    double resultado=1.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema
    if(numero)
    {
        resultado = ProductoLog((size_t)1,numero);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
long double FactorialLogLD(size_t numero)
{
    //variable para almacenar el resultado
    long double resultado=1.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //resolvemos el problema
    if(numero)
    {
        resultado = ProductoLogLD((size_t)1,numero);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
double Factorial(size_t numero)
{
    //variable para almacenar el resultado
    double resultado=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos el factorial
    if(numero<=GEOC_MATE_CONST_DBL_NMAXFAC)
    {
        resultado = (double)__tablaFacExpl[numero].valor;
    }
    else
    {
        //método de cálculo para números mayores que GEOC_MATE_CONST_DBL_NMAXFAC
#if defined(CALCULO_PRODUCTO_MULT)
        resultado = FactorialMult(numero);
#elif defined(CALCULO_PRODUCTO_LOG)
        resultado = FactorialLog(numero);
#else
    #error *****No se ha definido el método de cálculo de la función Factorial()
#endif
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
long double FactorialLD(size_t numero)
{
    //variable para almacenar el resultado
    long double resultado=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos el factorial
    if(numero<=GEOC_MATE_CONST_LDBL_NMAXFAC)
    {
        resultado = __tablaFacExpl[numero].valor;
    }
    else
    {
        //cálculo para números mayores que GEOC_MATE_CONST_LDBL_NMAXFAC
#if defined(CALCULO_PRODUCTO_MULT)
        resultado = FactorialMultLD(numero);
#elif defined(CALCULO_PRODUCTO_LOG)
        resultado = FactorialLogLD(numero);
#else
    #error *****No se ha definido el método de cálculo de la función Factorial()
#endif
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return resultado;
}
/******************************************************************************/
/******************************************************************************/
void ProductoVectorial(const double x1,
                       const double y1,
                       const double z1,
                       const double x2,
                       const double y2,
                       const double z2,
                       double* x,
                       double* y,
                       double* z)
{
    //calculamos las componentes
    *x = y1*z2-y2*z1;
    *y = x2*z1-x1*z2;
    *z = x1*y2-x2*y1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void SinCosRecurrencia(const double anguloIni,
                       const double incAngulo,
                       const size_t numValores,
                       double* seno,
                       const size_t incSeno,
                       double* coseno,
                       const size_t incCoseno)
{
    //índice para recorrer bucles
    size_t i=0;
    //posiciones en los vectores de trabajo
    size_t pS=0,pC=0;
    //variables auxiliares
    double aux=sin(incAngulo/2.0);
    double alfa=2.0*aux*aux,beta=sin(incAngulo);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //seno y coseno del primero elemento de la serie
    seno[pS] = sin(anguloIni);
    coseno[pC] = cos(anguloIni);
    //recorremos el resto de valores
    for(i=1;i<numValores;i++)
    {
        //calculamos las posiciones en los vectores de salida
        pS += incSeno;
        pC += incCoseno;
        //vamos calculando los senos y los cosenos
        seno[pS] = seno[pS-incSeno]-
                   (alfa*seno[pS-incSeno]-beta*coseno[pC-incCoseno]);
        coseno[pC] = coseno[pC-incCoseno]-
                     (alfa*coseno[pC-incCoseno]+beta*seno[pS-incSeno]);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void SplineCubicoNatural(double* x,
                         double* y,
                         size_t nDatos,
                         double* xInterp,
                         size_t nInterp)
{
    //sentencias para que no salgan avisos en la compilación
    x = x;
    y = y;
    nDatos = nDatos;
    xInterp = xInterp;
    nInterp = nInterp;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
