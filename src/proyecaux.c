/* -*- coding: utf-8 -*- */
/**
\ingroup gshhs geom proyec
@{
\file proyecaux.c
\brief Definición de funciones de algunas proyecciones cartográficas para no
       usar PROJ.4.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de agosto de 2013
\copyright
Copyright (c) 2013, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#include"libgeoc/proyecaux.h"
/******************************************************************************/
/******************************************************************************/
void ProjCilinEquivLambertLat0Ec(const double lat,
                                 const double lon,
                                 const double lon0,
                                 const double a,
                                 const double f,
                                 double* x,
                                 double* y)
{
    //variables auxiliares
    double k0=0.0,q=0.0,sLat=0.0,e=0.0,e2=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ASUMIMOS QUE EL PARALELO ORIGEN ES EL ECUADOR
    //distinguimos entre la esfera y el elipsoide
    if(f==0.0)
    {
        //proyectamos
        *x = a*(lon-lon0);
        *y = a*sin(lat);
    }
    else
    {
        //calculamos el seno de la latitud geodésica
        sLat = sin(lat);
        //calculamos la primera excentricidad del elipsoide
        e = sqrt(2.0*f-f*f);
        e2 = e*e;
        //el parámetro k0 porque el paralelo estándar es el ecuador
        k0 = 1.0;
        //calculamos el parámetro q
        q = (1.0-e2)*
            (sLat/(1.0-e2*sLat*sLat)-
             1.0/(2.0*e)*log((1.0-e*sLat)/(1.0+e*sLat)));
        //proyectamos
        *x = a*k0*(lon-lon0);
        *y = a*q/(2.0*k0);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
